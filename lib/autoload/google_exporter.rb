class GoogleExporter
  attr_accessor :config, :ws
  attr_reader :spreadsheet
  attr_reader :serialized_rows
  attr_reader :result

  def initialize(config=nil)
    @config = self.default_config.merge (config || {})
    @spreadsheet = spreadsheet
    @ws = spreadsheet.worksheet_by_title(@config[:ws_title])
  end

  def default_config
    @default_config = {
      ws_title: 'Предложения', #title your list
      spreadsheet_key: '1PxzQB8e7dk3gGM4l7QxEHR6oznpLiF-7qyBHGhVKS9Y' #token
    }
  end

  def serialize_rows!
    @serialized_rows = []
    (2..@ws.num_rows).each do |offer_row|
      next unless ws[offer_row, 6].present?
      @serialized_rows << {
        uid: ws[offer_row, 6],
        name: ws[offer_row, 6],
        google_drive_photos: ws[offer_row, 7],
        state: (serialize_state(ws[offer_row, 8]) || :undefined),
        room_type: (serialize_room_type(ws[offer_row, 9]) || :undefined),
        tower_id: serialize_tower(ws[offer_row, 10]),
        floor: ws[offer_row, 11].to_i,
        area: ws[offer_row, 12].to_f,
        price: Money.new(ws[offer_row, 13].gsub(/[^\d]/, '').to_i , (ws[offer_row, 14].present? ? ws[offer_row, 14] : 'RUB')),
        another_price: Money.new(ws[offer_row, 15].gsub(/[^\d]/, '').to_i , (ws[offer_row, 16].present? ? ws[offer_row, 16] : 'USD')),
        price_m2_rur_cents: ws[offer_row, 17].gsub(/[^\d]/, '').to_i,
        price_m2_usd_cents: ws[offer_row, 18].gsub(/[^\d]/, '').to_i,
        planner: serialize_planner(ws[offer_row, 19]),
        decorating: serialize_decorating(ws[offer_row, 20]),
        furniture: serialize_furniture(ws[offer_row, 21]),
        view: serialize_view(ws[offer_row, 22]),
        body: ws[offer_row, 23]
      }
    end
    @serialized_rows
  end

  def export!
    @result = []
    @serialized_rows = serialize_rows!
    ActiveRecord::Base.transaction do
      @serialized_rows.each do |offer_params|
        offer = Offer.where(uid: offer_params[:uid]).first_or_initialize
        offer.attributes = offer_params
        if offer.save
          @result << true
        else
          @result << false
          binding.pry
        end
      end
    end
  rescue
    binding.pry

  end

  def spreadsheet
    return @spreadsheet if @spreadsheet.present?
    @session = GoogleDrive.saved_session(Rails.root.to_s + "/config/config.json")
    @spreadsheet = @session.spreadsheet_by_key config[:spreadsheet_key]
  end

  def serialize_state(state_title)
    inverted_locale_hash = I18n.t('google_table.offer.state').invert
    inverted_locale_hash[state_title]
  end


  def serialize_room_type(room_type_title)
    inverted_locale_hash = I18n.t('google_table.offer.room_type').invert
    inverted_locale_hash[room_type_title]
  end

  def serialize_planner(plenner_title)
    inverted_locale_hash = I18n.t('google_table.offer.planner').invert
    inverted_locale_hash[plenner_title]
  end

  def serialize_furniture(furniture_title)
    inverted_locale_hash = I18n.t('google_table.offer.furniture').invert
    inverted_locale_hash[furniture_title]
  end

  def serialize_decorating(decorating_title)
    inverted_locale_hash = I18n.t('google_table.offer.decorating').invert
    inverted_locale_hash[decorating_title]
  end

  def serialize_view(view_title)
    inverted_locale_hash = I18n.t('google_table.offer.view').invert
    inverted_locale_hash[view_title]
  end

  def serialize_tower(tower_table_title)
    tower = Tower.where(table_title: tower_table_title).first_or_create(name: tower_table_title, table_title: tower_table_title)
    tower.id
  end

  private
end
