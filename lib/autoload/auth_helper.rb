module AuthHelper

  def authenticate_admin!
    not_authorized unless current_user.admin?
  end

  def not_authorized
    redirect_to(root_path, alert: 'Отказано в доступе')
  end

end
