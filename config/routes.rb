MoscowCity::Application.routes.draw do
  telegram_webhooks :moscowcity_bot => TelegramWebhooksController

  namespace :admin do
    resources :towers, except: :show do
      collection { post :batch_action }
    end

    resources :offers, except: :show do
      collection { post :batch_action }
      collection { post :image }
    end

    resources :offers_images, only: [:create, :destroy]

    resources :categories, except: :show do
      collection { post :batch_action }
      member { put :drop }
    end

    resources :user_companies, except: :show do
      collection { post :batch_action }
    end

    resources :helps, only: :index
    resource  :settings, only: [:edit, :update]

    resources :users, except: :show do
      collection { post :batch_action }
    end

    resources :versions, only: [:index, :show] do
      collection { post :batch_action }
    end

    resources :inline_attachments, only: :create
    resources :inline_images, only: [:new, :create, :destroy] do
      post 'add_item', on: :collection
    end

    resources :pages, except: :show do
      collection { post :batch_action }
      member { put :drop }
    end

    root to: 'pages#index'
  end

  namespace :ajax do
     resources :offers do
      collection do
        get :table
      end
    end
  end

  scope module: :web do
    # ActiveAdmin.routes(self)
    mount RedactorRails::Engine => 'redactor_rails'


    get :login, to: 'sessions#new'
    get :logout, to: 'sessions#destroy'


    resources :app_demands, only: [:index, :new, :create]
    resource :apps_list, only: [:show], controller: 'apps_list'
    resources :users
    resources :offers, only: [:index] do
      collection do
        get :imports
      end
    end
    resource :profile, only: [:show, :update]
    resource :session, only: [:create] do
      post :get_password
      get :registration, to: 'sessions#registration'
    end
    root to: 'main#show'
  end







  begin
    Page.for_routes.group_by(&:behavior).each do |behavior, pages|
      pages.each do |page|
        case behavior
        when nil
        else
          resource( "#{page.class.name.underscore}_#{page.id}",
                    path:       page.absolute_path,
                    controller: behavior,
                    only:       :show,
                    page_id:    page.id )
        end
      end
    end
  rescue
    nil
  end
end
