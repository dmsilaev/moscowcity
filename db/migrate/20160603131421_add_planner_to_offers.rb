class AddPlannerToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :planner, :string
  end
end
