class RemoveDecorateFromOffers < ActiveRecord::Migration
  def change
    remove_column :offers, :decorate
  end
end
