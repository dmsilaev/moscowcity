class AddAddPriceToOffers < ActiveRecord::Migration
  def change
    change_table :offers do |t|
      t.money :price    # Rails 3
      t.monetize :price # Rails 4x and above
      t.monetize :another_price
      t.monetize :price_m2_rur, currency: { present: false } # Rails 4x and above
      t.monetize :price_m2_usd, currency: { present: false }
    end
  end
end
