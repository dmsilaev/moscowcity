class RemoveBuyerRoleFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :buyer_role
  end
end
