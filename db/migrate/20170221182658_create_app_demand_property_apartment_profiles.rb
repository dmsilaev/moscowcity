class CreateAppDemandPropertyApartmentProfiles < ActiveRecord::Migration
  def change
    create_table :app_demand_property_apartment_profiles do |t|
      t.integer :min_rooms_count

      t.timestamps null: false
    end
  end
end
