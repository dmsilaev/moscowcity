class AddGoogleDrivePhotosToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :google_drive_photos, :string
  end
end
