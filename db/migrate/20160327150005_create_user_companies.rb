class CreateUserCompanies < ActiveRecord::Migration
  def change
    create_table :user_companies do |t|
      t.string   :name, null: false
      t.boolean  :published, default: true, null: false
      t.text     :meta_description
      t.string   :meta_title
      t.integer :user_id
      t.string :contact_name
      t.string :phone
      t.string :inn
      t.string :kpp
      t.string :rs
      t.string :bank_name
      t.string :ks
      t.string :bic

      t.timestamps null: false
    end
  end
end
