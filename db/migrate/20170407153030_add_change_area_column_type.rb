class AddChangeAreaColumnType < ActiveRecord::Migration
  def change
    change_column(:offers, :area, :float)
  end
end
