class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string   :name
      t.boolean  :published, default: true, null: false
      t.string   :slug
      t.text     :meta_description
      t.string   :meta_title
      t.integer  :parent_id
      t.integer  :depth, default: 0
      t.integer  :lft
      t.integer  :rgt
      t.string :name
      t.string :description
      t.string :image

      t.timestamps null: false
    end

    add_index :categories, :parent_id
    add_index :categories, :rgt
  end
end
