class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.string   :name
      t.boolean  :published, default: true, null: false
      t.string   :slug
      t.text     :meta_description
      t.string   :meta_title
      t.integer :number
      t.string :body
      t.string :state
      t.string :room_type
      t.string :currency
      t.integer :price
      t.integer :area
      t.integer :tower_id
      t.integer :floor
      t.string :view
      t.boolean :furniture
      t.boolean :decorate

      t.timestamps null: false
    end
  end
end
