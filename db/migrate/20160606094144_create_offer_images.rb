class CreateOfferImages < ActiveRecord::Migration
  def change
    create_table :offer_images do |t|
      t.integer :offer_id
      t.string :file

      t.timestamps null: false
    end
  end
end
