class DeleteCurrencyColumn < ActiveRecord::Migration
  def change
    remove_column :offers, :currency
  end
end
