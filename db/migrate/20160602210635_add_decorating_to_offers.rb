class AddDecoratingToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :decorating, :boolean, default: false
  end
end
