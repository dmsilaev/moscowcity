class AddClientRoleToUsers < ActiveRecord::Migration
  def change
    add_column :users, :client_role, :string
  end
end
