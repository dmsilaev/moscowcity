class CreateAppDemandPropertyOfficeProfiles < ActiveRecord::Migration
  def change
    create_table :app_demand_property_office_profiles do |t|
      t.integer :min_cabinets_count
      t.integer :max_cabinets_count
      t.integer :min_negotiation_count
      t.integer :max_negotiation_count
      t.string :open_space, null: false, default: 'any'

      t.timestamps null: false
    end
  end
end
