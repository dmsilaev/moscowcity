class CreateTowers < ActiveRecord::Migration
  def change
    create_table :towers do |t|
      t.string   :name
      t.boolean  :published, default: true, null: false
      t.string   :slug
      t.text     :meta_description
      t.string   :meta_title

      t.timestamps null: false
    end
  end
end
