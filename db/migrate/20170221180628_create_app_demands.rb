class CreateAppDemands < ActiveRecord::Migration
  def change
    create_table :app_demands do |t|
      t.string :type_of_deal, null: false, default: 'to_buy'
      t.string :type_of_property, null: false, default: 'office'
      t.integer :tower_id
      t.integer :user_id
      t.string :view, null: false, default: ''
      t.integer :floor_min, null: false, default: 0
      t.integer :floor_max, null: false, default: 100
      t.float :min_area, null: false, default: 10
      t.float :max_area, null: false, default: 1000
      t.string :decorate, null: false, default: 'any'
      t.string :furniture, null: false, default: 'any'

      t.string :deal_profile_type
      t.string :deal_profile_id

      t.string :property_profile_type
      t.string :property_profile_id

      t.timestamps null: false
    end
  end
end
