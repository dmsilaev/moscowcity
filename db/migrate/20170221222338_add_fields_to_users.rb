class AddFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :full_name, :string
    add_column :users, :phone, :string
    add_column :users, :site, :string
    add_column :users, :buyer_role, :string
  end
end
