class ChangeIntType < ActiveRecord::Migration
  def change
    change_column :offers, :price_cents, :bigint
    change_column :offers, :another_price_cents, :bigint
  end
end
