class RenameDecorateToDecorated < ActiveRecord::Migration
  def change
    rename_column :app_demands, :decorate, :decorated
  end
end
