class ChangeRequestColumnType < ActiveRecord::Migration
  def change
    change_column :offers, :request, :string
  end
end
