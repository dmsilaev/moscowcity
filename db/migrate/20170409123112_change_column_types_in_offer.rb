class ChangeColumnTypesInOffer < ActiveRecord::Migration
  def change
    change_column :offers, :decorating, :string
    change_column :offers, :furniture, :string
  end
end
