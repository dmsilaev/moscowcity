class CreateAppDemandDealToBuyProfiles < ActiveRecord::Migration
  def change
    create_table :app_demand_deal_to_buy_profiles do |t|
      t.float :max_cost
      t.float :max_cost_per_metr
      t.string :time_info

      t.timestamps null: false
    end
  end
end
