class CreateOfferSellProfiles < ActiveRecord::Migration
  def change
    create_table :offer_sell_profiles do |t|
      t.integer :price

      t.timestamps null: false
    end
  end
end
