class AddTableTitleToTowers < ActiveRecord::Migration
  def change
    add_column :towers, :table_title, :string
  end
end
