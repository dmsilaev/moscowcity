class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :title, null: false
      t.string :body, null: false
      t.string :keywords
      t.string :image

      t.timestamps null: false
    end
  end
end
