class AddRequestToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :request, :string
  end
end
