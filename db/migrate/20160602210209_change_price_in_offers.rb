class ChangePriceInOffers < ActiveRecord::Migration
  def change
    remove_column :offers, :price
    add_column :offers, :lease_price, :integer, null: false, default: 0
    add_column :offers, :sell_price, :integer, null: false, default: 0
  end
end
