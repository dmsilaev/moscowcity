class AddUidToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :uid, :string
    add_index :offers, :uid
  end
end
