class CreateAppDemandDealForRentProfiles < ActiveRecord::Migration
  def change
    create_table :app_demand_deal_for_rent_profiles do |t|
      t.date :first_rent_start_at
      t.date :last_rent_start_at
      t.string :rent_duration
      t.float :max_cost_per_month
      t.float :max_cost_per_m2_year

      t.timestamps null: false
    end
  end
end
