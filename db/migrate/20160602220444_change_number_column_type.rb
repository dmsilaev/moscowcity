class ChangeNumberColumnType < ActiveRecord::Migration
  def change
    change_column :offers, :number, :string
  end
end
