# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170413141021) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "app_demand_deal_for_rent_profiles", force: :cascade do |t|
    t.date     "first_rent_start_at"
    t.date     "last_rent_start_at"
    t.string   "rent_duration"
    t.float    "max_cost_per_month"
    t.float    "max_cost_per_m2_year"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "app_demand_deal_to_buy_profiles", force: :cascade do |t|
    t.float    "max_cost"
    t.float    "max_cost_per_metr"
    t.string   "time_info"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "app_demand_property_apartment_profiles", force: :cascade do |t|
    t.integer  "min_rooms_count"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "app_demand_property_office_profiles", force: :cascade do |t|
    t.integer  "min_cabinets_count"
    t.integer  "max_cabinets_count"
    t.integer  "min_negotiation_count"
    t.integer  "max_negotiation_count"
    t.string   "open_space",            default: "any", null: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "app_demands", force: :cascade do |t|
    t.string   "type_of_deal",          default: "to_buy", null: false
    t.string   "type_of_property",      default: "office", null: false
    t.integer  "tower_id"
    t.integer  "user_id"
    t.string   "view",                  default: "",       null: false
    t.integer  "floor_min",             default: 0,        null: false
    t.integer  "floor_max",             default: 100,      null: false
    t.float    "min_area",              default: 10.0,     null: false
    t.float    "max_area",              default: 1000.0,   null: false
    t.string   "decorated",             default: "any",    null: false
    t.string   "furniture",             default: "any",    null: false
    t.string   "deal_profile_type"
    t.string   "deal_profile_id"
    t.string   "property_profile_type"
    t.string   "property_profile_id"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  create_table "authentications", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "authentications", ["user_id"], name: "index_authentications_on_user_id", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.boolean  "published",        default: true, null: false
    t.string   "slug"
    t.text     "meta_description"
    t.string   "meta_title"
    t.integer  "parent_id"
    t.integer  "depth",            default: 0
    t.integer  "lft"
    t.integer  "rgt"
    t.string   "description"
    t.string   "image"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "categories", ["parent_id"], name: "index_categories_on_parent_id", using: :btree
  add_index "categories", ["rgt"], name: "index_categories_on_rgt", using: :btree

  create_table "news", force: :cascade do |t|
    t.string   "title",      null: false
    t.string   "body",       null: false
    t.string   "keywords"
    t.string   "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "offer_images", force: :cascade do |t|
    t.integer  "offer_id"
    t.string   "file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "offer_sell_profiles", force: :cascade do |t|
    t.integer  "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "offers", force: :cascade do |t|
    t.string   "name"
    t.boolean  "published",                                  default: true,    null: false
    t.string   "slug"
    t.text     "meta_description"
    t.string   "meta_title"
    t.string   "number"
    t.string   "body"
    t.string   "state"
    t.string   "room_type"
    t.float    "area"
    t.integer  "tower_id"
    t.integer  "floor"
    t.string   "view"
    t.string   "furniture"
    t.datetime "created_at",                                                   null: false
    t.datetime "updated_at",                                                   null: false
    t.integer  "lease_price",                                default: 0,       null: false
    t.integer  "sell_price",                                 default: 0,       null: false
    t.string   "decorating",                                 default: "false"
    t.string   "contact"
    t.string   "string"
    t.string   "request"
    t.string   "planner"
    t.string   "google_drive_photos"
    t.money    "price",                            scale: 2
    t.integer  "price_cents",            limit: 8,           default: 0,       null: false
    t.string   "price_currency",                             default: "USD",   null: false
    t.integer  "another_price_cents",    limit: 8,           default: 0,       null: false
    t.string   "another_price_currency",                     default: "USD",   null: false
    t.integer  "price_m2_rur_cents",                         default: 0,       null: false
    t.integer  "price_m2_usd_cents",                         default: 0,       null: false
    t.string   "uid"
  end

  add_index "offers", ["uid"], name: "index_offers_on_uid", using: :btree

  create_table "pages", force: :cascade do |t|
    t.string   "behavior",         default: "pages", null: false
    t.integer  "parent_id"
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "depth",            default: 0,       null: false
    t.string   "slug"
    t.string   "path"
    t.boolean  "nav_published",    default: false,   null: false
    t.boolean  "published",        default: false,   null: false
    t.string   "name"
    t.string   "nav_name"
    t.text     "body"
    t.text     "meta_description"
    t.string   "meta_title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pages", ["parent_id"], name: "index_pages_on_parent_id", using: :btree
  add_index "pages", ["rgt"], name: "index_pages_on_rgt", using: :btree

  create_table "redactor_assets", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "redactor_assets", ["assetable_type", "assetable_id"], name: "idx_redactor_assetable", using: :btree
  add_index "redactor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_redactor_assetable_type", using: :btree

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", null: false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "settings", force: :cascade do |t|
    t.string  "copyright"
    t.string  "email"
    t.string  "company_name"
    t.string  "contact_email"
    t.string  "email_header_from"
    t.text    "index_meta_description"
    t.string  "index_meta_title"
    t.integer "per_page",               default: 10
    t.text    "seo_google_analytics"
    t.text    "seo_yandex_metrika"
  end

  create_table "towers", force: :cascade do |t|
    t.string   "name"
    t.boolean  "published",        default: true, null: false
    t.string   "slug"
    t.text     "meta_description"
    t.string   "meta_title"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "table_title"
  end

  create_table "user_companies", force: :cascade do |t|
    t.string   "name",                            null: false
    t.boolean  "published",        default: true, null: false
    t.text     "meta_description"
    t.string   "meta_title"
    t.integer  "user_id"
    t.string   "contact_name"
    t.string   "phone"
    t.string   "inn"
    t.string   "kpp"
    t.string   "rs"
    t.string   "bank_name"
    t.string   "ks"
    t.string   "bic"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  create_table "users", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                  default: "",             null: false
    t.string   "encrypted_password",     default: "",             null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,              null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.integer  "roles_mask"
    t.string   "full_name"
    t.string   "phone"
    t.string   "site"
    t.string   "userpic"
    t.string   "crypted_password"
    t.string   "salt"
    t.string   "state",                  default: "unregistered"
    t.string   "telegram_login"
    t.string   "client_role"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
