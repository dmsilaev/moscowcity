module LoginHelpers
  def login_admin
    @admin = create :user
    @admin.roles << :admin
    auto_login(@admin)
  end
end
