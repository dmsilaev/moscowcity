require 'rails_helper'

RSpec.describe Web::MainController, type: :controller do

  describe "GET #show" do
    context 'logged in' do
      login_admin
      it "returns http success" do
        get :show
        expect(response).to be_success
      end
    end

    context 'not logged in' do
      it "returns redirect" do
        get :show
        expect(response).to be_redirect
      end
    end
  end

end
