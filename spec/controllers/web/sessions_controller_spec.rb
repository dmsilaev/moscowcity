require 'rails_helper'

RSpec.describe Web::SessionsController, type: :controller do

  describe "get new" do
    it "returns http success" do
      get :new
      expect(response).to be_success
    end
  end

  describe "request password" do
    it "return password form" do
      post :get_password, phone: '+7(921)560-71-23'
      expect(response).to be_success
    end

    it "return phone error" do
      post :get_password, phone: '***'
      expect(response).to be_success
    end
  end

end
