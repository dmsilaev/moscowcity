require 'rails_helper'

RSpec.describe Web::AppDemandsController, type: :controller do
  login_user

  let(:app_demands_attrs)     { attrs(:app_demand) }

  describe 'GET #new'
    it 'responds with 200'
      get :new
      expect(response).to be_success
    end
  end

  describe 'POST #create'
    it 'responds with 302'
      get :new
      expect(response).to be_success
    end
  end

end
