FactoryGirl.define do
  factory :search_request_deal_to_buy_profile, class: 'SearchRequest::DealToBuyProfile' do
    max_cost ""
    max_cost_per_metr 1.5
  end
end
