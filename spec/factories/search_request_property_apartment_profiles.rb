FactoryGirl.define do
  factory :search_request_property_apartment_profile, class: 'SearchRequest::PropertyApartmentProfile' do
    min_rooms_count 1
  end
end
