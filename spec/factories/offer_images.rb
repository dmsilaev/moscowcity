FactoryGirl.define do
  factory :offer_image, class: 'Offer::Image' do
    offer_id 1
    file "MyString"
  end
end
