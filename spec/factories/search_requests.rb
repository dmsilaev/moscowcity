FactoryGirl.define do
  factory :search_request do
    type_of_deal "MyString"
    type_of_property "MyString"
    tower_id 1
    view "MyString"
    floor_min 1
    floor_max 1
    min_area 1.5
    max_area 1.5
    decorate "MyString"
    furniture "MyString"
  end
end
