FactoryGirl.define do
  factory :search_request_deal_for_rent_profile, class: 'SearchRequest::DealForRentProfile' do
    first_rent_start_at "2017-02-21"
    last_rent_start_at "2017-02-21"
    rent_duration "MyString"
    max_cost_per_month 1.5
    max_cost_per_m2_year 1.5
  end
end
