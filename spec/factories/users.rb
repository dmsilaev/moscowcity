FactoryGirl.define do
  factory :user, aliases: [:creator] do
    login { generate :string }
    phone
    password {generate :string }
    password_confirmation { password }

    after(:build) do |u|
      u.profile = create :profile
    end
  end
end
