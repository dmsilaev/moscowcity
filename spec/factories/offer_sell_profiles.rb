FactoryGirl.define do
  factory :offer_sell_profile, class: 'Offer::SellProfile' do
    price 1
  end
end
