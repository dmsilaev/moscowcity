FactoryGirl.define do
  factory :search_request_property_office_profile, class: 'SearchRequest::PropertyOfficeProfile' do
    min_cabinets_count 1
    max_cabinets_count 1
    min_negotiation_count 1
    max_negotiation_count 1
    open_space "MyString"
  end
end
