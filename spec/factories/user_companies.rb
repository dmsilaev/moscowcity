FactoryGirl.define do
  factory :user_company do
    user_id 1
    name "MyString"
    contact_name "MyString"
    phone "MyString"
    inn "MyString"
    kpp "MyString"
    rs "MyString"
    bank_name "MyString"
    ks "MyString"
    bic "MyString"
  end
end
