FactoryGirl.define do
  factory :offer do
    number 1
    body "MyString"
    state "MyString"
    room_type "MyString"
    currency "MyString"
    price 1
    area 1
    tower_id 1
    floor 1
    view "MyString"
    furniture false
    decorate false
  end
end
