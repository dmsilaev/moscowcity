require 'rails_helper'
require 'autoload/google_exporter'

RSpec.describe 'GoogleExporter' do
  before do
    @token = GoogleExporter.new({}).default_config[:spreadsheet_key]
    @exporter = GoogleExporter.new({})
  end

  describe 'initialization object' do
    it 'attr_acessors' do
      expect(@config.merge GoogleExporter.new({}).default_config).to eq(@exporter.config)
      expect(@token).to eq(@exporter.token)
    end
  end

  describe 'export' do
    it 'all export' do
      @exporter.export!
      expect(@exporter.result.select { |m| m==true }.count).to eq(@exporter.serialized_rows.count)
    end
  end

end
