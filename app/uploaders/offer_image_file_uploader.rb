class OfferImageFileUploader < CommonUploader
  include CarrierWave::MiniMagick

  version :preview do
    process resize_to_fill: [75, 75]
  end

  version :slider do
    process resize_to_fill: [650, 650]
  end
end
