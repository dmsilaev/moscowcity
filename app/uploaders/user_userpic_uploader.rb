class UserUserpicUploader < CommonUploader
  include CarrierWave::MiniMagick

  version :thumb do
    process resize_to_fill: [75, 75]
  end

  version :preview do
    process resize_to_fill: [75, 75]
  end


  def extension_white_list
    %w(jpg jpeg gif png)
  end
end
