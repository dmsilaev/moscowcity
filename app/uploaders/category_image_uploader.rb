class CategoryImageUploader < CommonUploader
  include CarrierWave::MiniMagick

  version :preview do
    process resize_to_fill: [75, 75]
  end

  version :slidebanner

  def extension_white_list
    %w(jpg jpeg gif png)
  end
end
