class TelegramWebhooksController < Telegram::Bot::UpdatesController
  include ActionView::Helpers::NumberHelper
  NOTIFICATIONS_CHANNEL= '-189427669'

  include Telegram::Bot::UpdatesController::MessageContext
  context_to_action!

  def start(*)
    session[:form] = { }
    select_state
  end

  def import
    return unless self.payload['chat']['id'] != NOTIFICATIONS_CHANNEL
    e = GoogleExporter.new
    e.export!
    respond_with(:message, text: t('.ok'))
  rescue
    respond_with(:message, text: t('.not_ok'))
  end


  def select_state(*args)
    value = args.join(' ')
    unless value.present?
      save_context(:select_state)
      return respond_with(:message, text: t('.message'),
                                    reply_markup: markup_keyboard([t('.buttons').values])
                         )
    end
    return select_state unless t('.buttons').values.include?(value)

    value_key =  t('.buttons').select { |k,v| v == value }.keys.first
    session[:form].merge!(state: value_key.to_s) if [:sell, :lease].include?(value_key)
    select_room_type
  end

  def select_room_type(*args)
    value = args.join(' ')
    unless value.present?
      save_context(:select_room_type)
      return respond_with(:message, text: t('.message'),
                                    reply_markup: markup_keyboard([t('.buttons').values])
                         )
    end

    return select_room_type unless t('.buttons').values.include?(value)

    value_key =  t('.buttons').select { |k,v| v == value }.keys.first
    session[:form].merge!(room_type: value_key.to_s) if value_key != :any
    select_min_area
  end

  def select_min_area(*args)
    value = args.join(' ')
    unless value.present?
      save_context(:select_min_area)
      return respond_with(:message, text: t('.message'),
                                    reply_markup: markup_keyboard([t('.buttons').values])
                          )
    end
    value_key =  t('.buttons').select { |k,v| v == value }.keys.first

    return select_max_area if value_key == :any
    return select_min_area unless is_numeric?(value)

    session[:form].merge!(min_area: value.to_i)
    select_max_area
  end

  def select_max_area(*args)
    value = args.join(' ')
    unless value.present?
      save_context(:select_max_area)
      return respond_with(:message, text: t('.message'),
                                    reply_markup: markup_keyboard([t('.buttons').values])
                          )
    end
    value_key =  t('.buttons').select { |k,v| v == value }.keys.first

    return select_furniture if value_key == :any
    return select_max_area unless is_numeric?(value)

    session[:form].merge!(max_area: value.to_i)
    select_furniture
  end

  def select_furniture(*args)
    value = args.join(' ')
    unless value.present?
      save_context(:select_furniture)
      return respond_with(:message, text: t('.message'),
                                    reply_markup: markup_keyboard([t('.buttons').values])
                          )
    end
    value_key =  t('.buttons').select { |k,v| v == value }.keys.first

    return select_furniture unless t('.buttons').values.include?(value)
    return select_decorating if value_key == :any

    session[:form].merge!(furniture: value_key)
    select_decorating
  end

  def select_decorating(*args)
    value = args.join(' ')
    unless value.present?
      save_context(:select_decorating)
      return respond_with(:message, text: t('.message'),
                                    reply_markup: markup_keyboard([t('.buttons').values])
                          )
    end
    value_key =  t('.buttons').select { |k,v| v == value }.keys.first

    return select_decorating unless t('.buttons').values.include?(value)
    return select_type_max_price if value_key == :any

    session[:form].merge!(decorating: value_key)
    select_type_max_price
  end

  def select_type_max_price
    return select_max_rent_price if session[:form][:state] == 'lease'
    return select_max_price if session[:form][:state] == 'sell'
  end

   def select_max_rent_price(*args)
    value = args.join(' ')
    unless value.present?
      save_context(:select_max_rent_price)
      return respond_with(:message, text: t('.message'),
                                    reply_markup: markup_keyboard([t('.buttons').values])
                          )
    end
    value_key =  t('.buttons').select { |k,v| v == value }.keys.first

    return search if value_key == :any
    return select_max_rent_price unless is_numeric?(value)

    session[:form].merge!(max_price: value.to_i)
    search
  end

  def select_max_price(*args)
    value = args.join(' ')
    unless value.present?
      save_context(:select_max_price)
      return respond_with(:message, text: t('.message'),
                                    reply_markup: markup_keyboard([t('.buttons').values])
                          )
    end
    value_key =  t('.buttons').select { |k,v| v == value }.keys.first

    return search if value_key == :any
    return select_max_price unless is_numeric?(value)

    session[:form].merge!(max_price: value.to_i)
    search
  end

  def search
    form = session[:form]
    search = Offer.solr_search do
      with(:state, form[:state]) if form[:state].present?
      with(:room_type, [form[:room_type], 'apart_office']) if form[:room_type].present?
      with(:furniture, ['yes','optional']) if form[:furniture].present?
      with(:decorating, ['yes','optional']) if form[:decorating].present?
      with(:area).greater_than form[:min_area] if form[:min_area].present?
      with(:area).less_than form[:max_area] if form[:max_area].present?
      with(:price).less_than form[:max_price] if form[:max_price].present?
      paginate :page => 1, :per_page => 10
      order_by :price, :asc
    end

    results = search.results
    return respond_with(:message, text: t('.not_found')) if results.count == 0

    complete_form
    respond_with(:message, text: results_message(results))
  end

  def results_message(results)
    results.map do |result|
      str =''
      str += "- #{result.state_text}, #{result.room_type_text}, #{result.tower.name}, "
      str += "#{result.floor} этаж, " if result.floor.present?
      str += "#{result.area} м2, " if result.area.present?

      price_symbol = result.price.currency.symbol

      if result.state == 'lease'
        price = number_to_currency((result.price_cents/100), unit: "#{price_symbol}/мес", separator: "", precision: 0, delimiter: ".", format: "%n %u")
        if result.price_currency == 'RUB'
          price_m2 = number_to_currency((result.price_m2_rur_cents/100), unit: "#{price_symbol}/м2/год", precision: 0, separator: "", delimiter: ".", format: "%n %u")
        else
          price_m2 = number_to_currency((result.price_m2_usd_cents/100), unit: "#{price_symbol}/м2/год", precision: 0, separator: "", delimiter: ".", format: "%n %u")
        end
      elsif result.state == 'sell'
        price = number_to_currency((result.price_cents/100), unit: "#{price_symbol}", precision: 0,eparator: "", delimiter: ".", format: "%n %u")
        if result.price_currency == 'RUB'
          price_m2 = number_to_currency((result.price_m2_rur_cents/100), unit: "#{price_symbol}/м2",  precision: 0,separator: "", delimiter: ".", format: "%n %u")
        else
          price_m2 = number_to_currency((result.price_m2_usd_cents/100), unit: "#{price_symbol}/м2",  precision: 0,separator: "", delimiter: ".", format: "%n %u")
        end
      end

      str += "#{price} (#{price_m2})"
      str += ", #{result.planner_text}" if result.planner.present?
      str += ", отделка: #{result.furniture_text}" if result.furniture.present?
      str += ", мебель: #{result.decorating_text}" if result.decorating.present?
      str += ", фото: #{result.google_drive_photos}" if result.google_drive_photos.present?
      str.mb_chars.downcase.to_s
    end.join("\n\n")
  end

  def reestr(*query)
    query = query.join('')
    json = rosreestr.call('cadaster/search', {query: query})
    json['objects']
    respond_with :message, text: "`#{json.to_json}`"
  end

  def complete_form
    user = self.from.symbolize_keys
    text = "@#{user[:username]}(#{user[:first_name]} #{user[:last_name]}) отправил новый запрос:\n"
    text += "— #{session[:form].to_json}"
    bot.send_message chat_id: NOTIFICATIONS_CHANNEL, text: text
  end

  def help(*)
    respond_with :message, text: <<-TXT.strip_heredoc
      Available cmds:
      /memo %text% - Saves text to session.
      /remind_me - Replies with text from session.
      /keyboard - Simple keyboard.
      /inline_keyboard - Inline keyboard example.
      Bot supports inline queries. Enable it in @BotFather.
      /last_chosen_inline_result - Your last chosen inline result \
      (Enable feedback with /setinlinefeedback).
    TXT
  end

  def memo(*args)
    if args.any?
      session[:memo] = args.join(' ')
      respond_with :message, text: 'Remembered!'
    else
      respond_with :message, text: 'What should I remember?'
      save_context :memo
    end
  end

  def remind_me
    to_remind = session.delete(:memo)
    reply = to_remind || 'Nothing to remind'
    respond_with :message, text: reply
  end

  def keyboard(value = nil, *)
    if value
      respond_with :message, text: "You've selected: #{value}"
    else
      save_context :keyboard
      respond_with :message, text: 'Select something with keyboard:', reply_markup: {
        keyboard: [%w(Lorem Ipsum /cancel)],
        resize_keyboard: true,
        one_time_keyboard: true,
        selective: true,
      }
    end
  end

  def inline_keyboard
    respond_with :message, text: 'Check my inline keyboard:', reply_markup: {
      inline_keyboard: [
        [
          {text: 'Answer with alert', callback_data: 'alert'},
          {text: 'Without alert', callback_data: 'no_alert'},
        ],
        [{text: 'Open gem repo', url: 'https://github.com/telegram-bot-rb/telegram-bot'}],
      ],
    }
  end

  def callback_query(data)
    if data == 'alert'
      answer_callback_query 'This is ALERT-T-T!!!', show_alert: true
    else
      answer_callback_query 'Simple answer'
    end
  end

  def message(message)
    respond_with :message, text: "You wrote: #{message['text']}"
  end

  def inline_query(query, offset)
    query = query.first(10) # it's just an example, don't use large queries.
    results = 5.times.map do |i|
      {
        type: :article,
        title: "#{query}-#{i}",
        id: "#{query}-#{i}",
        description: "description #{i}",
        input_message_content: {
          message_text: "content #{i}",
        },
      }
    end
    answer_inline_query results
  end

  # As there is no chat id in such requests, we can not respond instantly.
  # So we just save the result_id, and it's available then with `/last_chosen_inline_result`.
  def chosen_inline_result(result_id, query)
    session[:last_chosen_inline_result] = result_id
  end

  def last_chosen_inline_result
    result_id = session[:last_chosen_inline_result]
    if result_id
      respond_with :message, text: "You've chosen result ##{result_id}"
    else
      respond_with :message, text: 'Mention me to initiate inline query'
    end
  end

  def action_missing(action, *_args)
    if command?
      respond_with :message, text: "Can not perform #{action}"
    else
      respond_with :message, text: "#{action} feature is not implemented yet"
    end
  end

  private

  def t(key, options = {})
    if key.to_s.first == '.'
      path = controller_path.tr('/', '.')
      defaults = [:"#{path}#{key}"]
      defaults << options[:default] if options[:default]
      options[:default] = defaults
      key = "#{path}.#{caller[0][/`([^']*)'/, 1]}#{key}"
    end
    I18n.translate(key, options)
  end

  def is_numeric?(obj)
    obj.to_s.match(/\A[+-]?\d+?(\.\d+)?\Z/) == nil ? false : true
  end

  def rosreestr
    @rosreestr ||= Apirosreestr::Api.new('DFJ3-B4SN-D9BN-LPEA')
  end

  def form_last_step?
    session[:form][:step] > session[:form][:max_step]
  end

  def markup_keyboard(keyboard, *_args)
    {
      keyboard: keyboard,
      resize_keyboard: true,
      one_time_keyboard: true,
      selective: true,
    }
  end
end
