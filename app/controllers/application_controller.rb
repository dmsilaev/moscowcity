class ApplicationController < ActionController::Base
  # protect_from_forgery
  include Pundit
  include FlashHelper

  respond_to :html

  before_action :check_routes_version
  before_action :ajax_sets


  def add_ajax_url(path, document_action, *args)
    @options = args.extract_options!
    @ajax_sets ||= []

    @ajax_sets << {
                    document_action: document_action,
                    url: path,
                    method: options[:method] || 'GET'
                  }
  end

  def ajax_sets
    @ajax_sets ||= []
  end
  helper_method :ajax_sets



  protected

  rescue_from CanCan::AccessDenied do |exception|
    path = current_user ? root_path : new_user_session_path
    session[:previous_url] = request.fullpath
    redirect_to path, alert: exception.message
  end

  def after_sign_in_path_for resource
    session[:previous_url] || super
  end

  def check_routes_version
    version = Rails.cache.fetch(:routes_version) { 0 }
    ApplicationController.update_routes(version) if version != @@routes_version
  end

  def paper_trail_enabled_for_controller
    ApplicationController.descendants.include? Admin::BaseController
  end
end
