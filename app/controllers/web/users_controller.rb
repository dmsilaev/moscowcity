class Web::UsersController < Web::ApplicationController
  load_and_authorize_resource

  def index
    @users = User.all
  end

  def new
    @form = AdminUserForm.new(User.new)
  end

  def create
    @form = AdminUserForm.new(User.new)

    if @form.validate(params[:admin_user]) && @form.save
      f(:success, full_name: @form.full_name)
      redirect_to(users_path)
    else
      f(:error)
      render :new, status: :bad_request
    end
  end

  def edit
    @user = User.find(params[:id])
    @form = AdminUserForm.new(@user)
  end

  def update
    @user = User.find(params[:id])
    @form = AdminUserForm.new(@user)

    if @form.validate(params[:admin_user]) && @form.save
      f(:success, full_name: @form.full_name)
      redirect_to(users_path)
    else
      f(:error)
      render :edit, status: :bad_request
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    f(:success, full_name: @user.full_name)
    redirect_to(users_path)
  end

end
