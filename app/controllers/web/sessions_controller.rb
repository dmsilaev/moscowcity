class Web::SessionsController < Web::ApplicationController
  layout 'login'
  skip_before_action :authenticate_user!
  skip_before_action :check_registration!
  before_action :to_main_if_logged_in, only: :new

  def get_password
    @phone = params[:phone]
    unless @phone =~ /\+7\([0-9]{3}\)[0-9]{3}-[0-9]{2}-[0-9]{2}/
      f(:wrong_phone)
      return(render :new)
    end

    @user = User.find_or_create_by(phone: @phone)
    @user.regenerate_password!
    @form = UserLoginForm.new(User.new(phone: @phone))
    return render(:login_form)
  end

 def new
    @form = UserLoginForm.new(User.new(phone: @phone))
    return render(:login_form)
  end

  def create
    @form = UserLoginForm.new(User.new)

    if @form.validate(params[:user_login]) &&
      login(params[:user_login][:phone], params[:user_login][:password], remember_me: true)
      f(:success, panel_link: ActionController::Base.helpers.link_to('сюда', root_path))
      render :success
    else
      f(:error)
      render :get_password, status: :unauthorized
    end
  end

  def destroy
    logout
    redirect_to new_session_path
  end

  def registration
    @form = UserProfileForm.new(User.new)
    render :registration_form
  end

  def destroy
    logout
    f(:success)
    redirect_to login_path
  end

  private

  def to_main_if_logged_in
    redirect_to(root_path) if logged_in?
  end
end
