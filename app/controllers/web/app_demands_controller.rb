class Web::AppDemandsController < Web::ApplicationController

  def new
    @form = AppDemandForm.new(AppDemand.new)
  end

  def create
    @form = AppDemandForm.new(current_user.app_demands.new)
    if @form.validate(params[:app_demand].permit!) && @form.prepopulate! && @form.save
      f(:success)
      redirect_to url_for(action: :index)
    else
      f(:error)
      render :new
    end
  end
end
