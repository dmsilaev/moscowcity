class Web::ApplicationController < ApplicationController
  layout 'material'
  before_action :authenticate_user!
  before_action :check_registration!

  @@routes_version = 0

  def self.update_routes version
    @@routes_version = version
    Rails.application.reload_routes!
  end

  protected

  def authenticate_user!
    unless logged_in?
      flash[:message] = "Необходимо войти"
      redirect_to login_path
    end
  end

  def check_registration!
    redirect_to registration_session_path unless current_user.full_name.present?
  end

  rescue_from CanCan::AccessDenied do |exception|
    path = current_user ? root_path : new_user_session_path
    session[:previous_url] = request.fullpath
    redirect_to path, alert: exception.message
  end

  def after_sign_in_path_for resource
    session[:previous_url] || super
  end

  def check_routes_version
    version = Rails.cache.fetch(:routes_version) { 0 }
    ApplicationController.update_routes(version) if version != @@routes_version
  end

  def paper_trail_enabled_for_controller
    ApplicationController.descendants.include? Admin::BaseController
  end
end
