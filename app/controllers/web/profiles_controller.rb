class Web::ProfilesController < Web::ApplicationController
  skip_before_action :check_registration!, only: [:update]

  def show
    @profile = current_user
  end

  def edit
    @form = UserProfileForm.new(current_user)
  end

  def update
    @form = UserProfileForm.new(current_user)
    if @form.validate(params[:user_profile]) && @form.save
      redirect_to root_path
    else
      rendirect_to :edit
    end
  end

end
