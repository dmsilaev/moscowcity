class Admin::PagesController < Admin::BaseController
  resource( Page,
            location: proc { polymorphic_path([:admin, resource.class]) },
            collection_scope: [:sorted] )
end
