class Admin::BaseController < ApplicationController
  include Adminos::Controllers::AdminExtension
  layout 'material'

  before_action :authorize, :define_breadcrumb_struct

   def add_ajax_url(path, document_action, *args)
    @options = args.extract_options!
    @ajax_sets ||= []

    @ajax_sets << {
                    document_action: document_action,
                    url: path,
                    method: options[:method] || 'GET'
                  }
  end

  def ajax_sets
    @ajax_sets ||= []
  end
  helper_method :ajax_sets

  private

  def authorize
    authorize! :manage, :all
  end

  def define_breadcrumb_struct
    Struct.new('Breadcrumb', :label, :url) unless defined?(Struct::Breadcrumb)
  end
end
