class Admin::OffersImagesController < ApplicationController
  def create
    @image = Offer::Image.new()
    @image.file = params[:sfile][:upload].first

    @image.save
    render layout: false
  end

  def destroy
    @image = Offer::Image.find(params[:id])

    @image.destroy
    render layout: false
  end
end
