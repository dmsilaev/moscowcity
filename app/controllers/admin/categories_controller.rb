class Admin::CategoriesController < Admin::BaseController
  resource( Category,
            collection_scope: [:sorted]
          )
end
