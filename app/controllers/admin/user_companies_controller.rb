class Admin::UserCompaniesController < Admin::BaseController
  resource( UserCompany,
            collection_scope: [:sorted],
            location: proc { polymorphic_path([:admin, resource.class]) },
            finder: :find_by_slug! )

  private

  alias_method :collection_orig, :collection
  def collection
    @collection ||= collection_orig
      .page(params[:page]).per(settings.per_page)
  end
end
