class Admin::TowersController < Admin::BaseController
  resource( Tower,
            collection_scope: [:sorted],
            location: proc { polymorphic_path([:admin, resource.class]) })

  private

  alias_method :collection_orig, :collection
  def collection
    @collection ||= collection_orig
      .page(params[:page]).per(settings.per_page)
  end
end
