class IndexController < ApplicationController
  layout 'material'

  def index
    @query = query
    @collection = []
  end

  def search
    @search = Offer.search where: @query,
                             per_page: 10,
                             page: 1
    @collection = @search.results
  end

  private

  def query
    @query = {'type' => 'sell', 'currency_rur'=> true, 'currency_usd' => true}
    params[:q] ||= {}

    if params[:q].present?
      @query.merge!({ 'type' => params[:q][:type] })
      @query.merge!({ 'currency_rur' => params[:q][:currency_rur]=='1', 'currency_usd' => params[:q][:qurrency_usd]=='1' })
    end

    case @query['type']
    when 'lease'
      @query.merge!('price' => 0..(params[:q][:max_budget] && params[:q][:max_budget].to_i|| Offer.order(:lease_price).last.lease_price))
    when 'sell'
      @query.merge!('price' => 0..(params[:q][:max_budget] && params[:q][:max_budget].to_i || Offer.order(:sell_price).last.sell_price))
    end

    @query
  end

  def default_query
    @default_query ||= {
      'type'=> 'sell',
      'price' => 0..Offer.order(:sell_price).last.sell_price,
      'currency' => ['rur','usd']
    }
  end
end
