class AdminUserForm < Reform::Form
  property :phone
  property :full_name
  property :site
  property :email
  property :userpic
  property :roles
  property :password

  validates :phone, presence: true
  validates :full_name, presence: true
  validates :password, presence: true, allow_blank: true
end
