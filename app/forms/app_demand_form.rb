class AppDemandForm < Reform::Form
  property :type_of_deal,  prepopulator: ->(options) { assign_deal_profile(options) }
  validates :type_of_deal, presence: true

  property :type_of_property, prepopulator: ->(options) { assign_property_profile(options) }
  validates :type_of_property, presence: true

  property :tower_id

  property :user_id
  validates :user_id, presence: true

  property :view, default: '–'
  property :floor_min
  property :floor_max
  property :min_area
  property :max_area
  property :decorated, default: '–'
  property :furniture
  properties :to_buy_attributes, :for_rent_attributes, :apartment_attributes, :office_attributes, virtual: true



  property :client, populate_if_empty: lambda { |fragment, args| User.new } do
    property :full_name
    validates :full_name, presence: true

    property :phone
    validates :phone, presence: true

    property :email
    validates :email, presence: true

    property :site, default: '–'
    property :client_role
  end




  def assign_deal_profile(options)
    case type_of_deal
    when 'to_buy'
      model.deal_profile = AppDemand::DealToBuyProfile.new(to_buy_attributes)
    when 'for_rent'
      model.deal_profile = AppDemand::DealForRentProfile.new(for_rent_attributes)
    else
      nil
    end
  end

  def assign_property_profile(options)
    case type_of_property
    when 'apartment'
      property_profile = AppDemand::PropertyApartmentProfile.new(apartment_attributes)
    when 'office'
      property_profile = AppDemand::PropertyOfficeProfile.new(office_attributes)
    else
      nil
    end
  end

end

