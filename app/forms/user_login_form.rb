class UserLoginForm < Reform::Form
  property :phone
  property :password

  validates :phone, presence: true
  validates :password, presence: true
end
