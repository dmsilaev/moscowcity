class UserProfileForm < Reform::Form
  # property :buyer_role
  property :full_name
  property :site
  property :userpic

  # validates :buyer_role, presence: true
  validates :full_name, presence: true
end
