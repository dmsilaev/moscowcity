//= require jquery
//= require jquery_ujs
//= require jquery.cookie

// Bower packages
//= require jquery/dist/jquery.min
//= require bootstrap/dist/js/bootstrap.min

//= require flot/jquery.flot
//= require flot/jquery.flot.resize
//= require flot.curvedlines/curvedLines
//= require jquery.easy-pie-chart/dist/jquery.easypiechart.min

//= require moment/min/moment.min
//= require simpleWeather/jquery.simpleWeather.min
//= require Waves/dist/waves.min
//= require input-mask/input-mask.min
//= require sweetalert/dist/sweetalert.min
//= require malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min
//= require lightgallery/dist/js/lightgallery-all.min

//= require app
