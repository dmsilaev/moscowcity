$ ->
  # Show authentication modal if cookie is present
  if $.cookie('link_auth_modal')
    $('#authenticationModal').modal()
    $.removeCookie 'link_auth_modal'

  $('body').on 'submit', 'form#link_auth', (e) ->
    e.preventDefault()
    form = $(this)

    $.ajax
      url: form.attr('action')
      type: 'POST'
      data: form.serialize()
      success: ->
        location.reload()
      error: (xhr, status, thrownError) ->
        message = if xhr.status == 401
          'Неправильный логин или пароль'
        else
          'Произошла непредвиденная ошибка'

        $('.link_auth-message').html message
