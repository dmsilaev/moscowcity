//=require jquery.bootgrid/dist/jquery.bootgrid.min.js

$(document).ready(function(){
  //Command Buttons
  $("#data-table-users").bootgrid({
      css: {
          icon: 'zmdi icon',
          iconColumns: 'zmdi-view-module',
          iconDown: 'zmdi-sort-amount-desc',
          iconRefresh: 'zmdi-refresh',
          iconUp: 'zmdi-sort-amount-asc'
      }
  });
});
