class CarrierwaveInput < SimpleForm::Inputs::Base
  def input wrapper_options
    html = []
    file = @builder.object.send(attribute_name)

    version = input_options.delete(:version) || :preview
    image   = if input_options.has_key?(:image)
                input_options.delete(:image)
              else
                file.image?
              end

    html << @builder.file_field(attribute_name, input_html_options)

    if file.present?
      if image
        html << %{<img src="#{file.send(version).url rescue ''}"/>}
      else
        html << %{<div>#{File.basename(file.path)}</div>}
      end
    end

    html.join.html_safe
  end
end
