class AppDemand::DealForRentProfile < ActiveRecord::Base
  extend Enumerize

  has_one :app_demand, :as => :deal_profile

  enumerize :rent_duration, in: [:few_months, :from_3_to_6, :from_6_to_year, :more_year]
end
