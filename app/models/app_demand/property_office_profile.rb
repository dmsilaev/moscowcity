class AppDemand::PropertyOfficeProfile < ActiveRecord::Base
  extend Enumerize

  has_one :app_demand, :as => :property_profile

  enumerize :open_space, in: [:any, :yes, :no], default: :any
end
