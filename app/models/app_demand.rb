class AppDemand < ActiveRecord::Base
  extend Enumerize

  belongs_to :tower
  belongs_to :user
  belongs_to :client, class_name: 'User'
  belongs_to :deal_profile, polymorphic: true
  belongs_to :property_profile, polymorphic: true

  enumerize :type_of_deal, in: [:to_buy, :for_rent]
  enumerize :type_of_property, in: [:apartment, :office]
  enumerize :decorated, in: [:any, :yes, :no], default: :any
  enumerize :furniture, in: [:any, :yes, :no], default: :any

  accepts_nested_attributes_for :client, :deal_profile, :property_profile

  scope :sorted, -> { order('created_at DESC') }

  belongs_to :tower
  validates :user, presence: true






end
