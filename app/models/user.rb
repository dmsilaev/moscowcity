require 'role_model'

class User < ActiveRecord::Base
  authenticates_with_sorcery!
  extend Enumerize
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  include RoleModel
  attr_accessor :sms_pass
  attr_accessor :admin_roles

  mount_uploader :userpic, UserUserpicUploader

  enumerize :admin_roles, in: [:admin, :worker, :client, :partner]
  enumerize :client_role, in: [:client, :partner]

  roles :admin, :worker, :client, :partner
  scoped_search on: :email

  validates :phone, presence: true

  has_many :app_demands

  def regenerate_password!
    generate_password
    save
  end

  def state_text
    '-'
  end

  def to_s
    full_name
  end

  def avatar_url
    userpic.present? ? userpic_url(:thumb) : "https://placehold.it/50/#{full_name.first}}/000?text=#{full_name.first}"
  end

  private


  def generate_password
    alphabet =  "ABCDEFGHJKMNPQRSTUVWXYZ23456789"

    self.password = loop do
      random_code = Array.new(5) { alphabet[rand(alphabet.length)] }.join('').upcase
      break random_code
    end
    self.sms_pass = self.password
  end


  def translated_roles
    role_symbols.map { |s| self.class.human_attribute_name(s) }.join ', '
  end
end
