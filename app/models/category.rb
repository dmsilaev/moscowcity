class Category < ActiveRecord::Base
  include Adminos::NestedSet::MaterializePath
  include Adminos::NestedSet::PlaceTo
  include Adminos::NestedSet::SafeDestroy
  include Adminos::Slugged
  include Adminos::Wysiwyg
  include Adminos::FlagAttrs

  mount_uploader :image, CategoryImageUploader


  acts_as_nested_set


  scope :sorted, -> { order('lft ASC') }

  validates :name, presence: true
  validates :image, presence: true, if: :published

  scope :top, -> { where(depth: 0).sorted }
  flag_attrs :published

end
