class Offer::Image < ActiveRecord::Base
  mount_uploader :file, OfferImageFileUploader

  belongs_to :offer
end
