class Tower < ActiveRecord::Base
  include Adminos::Slugged
  include Adminos::FlagAttrs

  slugged :recognizable_name
  flag_attrs :published

  validates :name, presence: true
  has_many :offers

  scope :sorted, -> { order('created_at DESC') }

  def reasonable_name
    name
  end

  def recognizable_name
    slug.present? ? slug : reasonable_name
  end
end
