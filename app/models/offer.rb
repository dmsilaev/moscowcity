class Offer < ActiveRecord::Base
  include Adminos::FlagAttrs
  extend Enumerize

  flag_attrs :published

  monetize :price_cents
  monetize :price_m2_rur_cents, currency: 'RUB'
  monetize :price_m2_usd_cents, currency: 'USD'
  monetize :another_price_cents

  validates :name, presence: true

  scope :sorted, -> { order('created_at DESC') }
  enumerize :state, in: [:lease, :sell, :closed_lease, :closed_sell, :rent, :sold, :with_realization, :not_active, :sell_explore, :lease_explore, :undefined]
  enumerize :room_type, in: [:office, :apart, :apart_office, :mm, :floor, :undefined, :torg]
  enumerize :planner, in: [:open_space, :сabinets, :open_space_cabinets, :no_rooms, :one_room, :two_rooms, :three_rooms, :more_rooms, :penthouse, :free]
  enumerize :furniture, in: [:yes, :no, :optional]
  enumerize :decorating, in: [:yes, :no, :optional]
  enumerize :view, in: [:south, :north, :east, :west, :northeast, :northwest, :southeast, :southwest, '360']

  scope :published, -> { where('lease, sell, closed_lease, closed_sell')}

  belongs_to :tower
  validates :state, presence: true
  validates :room_type, presence: true
  validates :area, presence: true
  validates :floor, presence: true

  has_many :images, class_name: 'Offer::Image'



  searchable do
    string :state
    string :room_type
    integer :price do
      (price_cents_rur/100)
    end
    integer :rent_price_rur do
      (price_m2_rur_cents/100)
    end
    integer :floor
    integer :area
    string :decorating
    string :furniture
  end



  def sqr_price
    price/area
  end

  def price_cents_rur
    return price_cents if price_currency == 'RUB'
    return another_price_cents if price_currency == 'USD'
  end

  def lease?
    self.state == 'lease' || self.state == 'closed_lease'
  end
  def sell?
    self.state == 'sell' || self.state == 'closed_sell'
  end

  def net
    (lease_price/area) * 12
  end

  def reasonable_name
    name
  end


end
