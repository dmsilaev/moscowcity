$modal = $("#terminalModal<%=@resource_terminal.id%>")
unless $modal.length
  $('.modal-block').html('<%= j render "modal" %>')
  $modal = $("#terminalModal<%=@resource_terminal.id%>")
$modal.modal()

$nav = $modal.find('.modal-body .nav')
$content = $modal.find('.modal-body .content')

$nav.html('<%= j render  "shared/terminal_modal/nav"%>')
$content.html('<%= j render "modal_content"%>')

$.fn.editable.defaults.ajaxOptions = {type: "PUT"};
$('.editable').editable()
