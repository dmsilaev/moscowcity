# -*- encoding: utf-8 -*-
# stub: google_drive 2.1.3 ruby lib

Gem::Specification.new do |s|
  s.name = "google_drive"
  s.version = "2.1.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Hiroshi Ichikawa"]
  s.date = "2017-04-06"
  s.description = "A library to read/write files/spreadsheets in Google Drive/Docs."
  s.email = ["gimite+github@gmail.com"]
  s.files = ["README.md", "lib/google_drive", "lib/google_drive.rb", "lib/google_drive/access_token_credentials.rb", "lib/google_drive/acl.rb", "lib/google_drive/acl_entry.rb", "lib/google_drive/api_client_fetcher.rb", "lib/google_drive/authentication_error.rb", "lib/google_drive/collection.rb", "lib/google_drive/config.rb", "lib/google_drive/error.rb", "lib/google_drive/file.rb", "lib/google_drive/list.rb", "lib/google_drive/list_row.rb", "lib/google_drive/response_code_error.rb", "lib/google_drive/session.rb", "lib/google_drive/spreadsheet.rb", "lib/google_drive/util.rb", "lib/google_drive/worksheet.rb"]
  s.homepage = "https://github.com/gimite/google-drive-ruby"
  s.licenses = ["BSD-3-Clause"]
  s.required_ruby_version = Gem::Requirement.new(">= 2.0.0")
  s.rubygems_version = "2.4.5.1"
  s.summary = "A library to read/write files/spreadsheets in Google Drive/Docs."

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<nokogiri>, ["< 2.0.0", ">= 1.5.3"])
      s.add_runtime_dependency(%q<google-api-client>, ["< 0.12.0", ">= 0.11.0"])
      s.add_runtime_dependency(%q<googleauth>, ["< 1.0.0", ">= 0.5.0"])
      s.add_development_dependency(%q<test-unit>, ["< 4.0.0", ">= 3.0.0"])
      s.add_development_dependency(%q<rake>, [">= 0.8.0"])
      s.add_development_dependency(%q<rspec-mocks>, ["< 4.0.0", ">= 3.4.0"])
    else
      s.add_dependency(%q<nokogiri>, ["< 2.0.0", ">= 1.5.3"])
      s.add_dependency(%q<google-api-client>, ["< 0.12.0", ">= 0.11.0"])
      s.add_dependency(%q<googleauth>, ["< 1.0.0", ">= 0.5.0"])
      s.add_dependency(%q<test-unit>, ["< 4.0.0", ">= 3.0.0"])
      s.add_dependency(%q<rake>, [">= 0.8.0"])
      s.add_dependency(%q<rspec-mocks>, ["< 4.0.0", ">= 3.4.0"])
    end
  else
    s.add_dependency(%q<nokogiri>, ["< 2.0.0", ">= 1.5.3"])
    s.add_dependency(%q<google-api-client>, ["< 0.12.0", ">= 0.11.0"])
    s.add_dependency(%q<googleauth>, ["< 1.0.0", ">= 0.5.0"])
    s.add_dependency(%q<test-unit>, ["< 4.0.0", ">= 3.0.0"])
    s.add_dependency(%q<rake>, [">= 0.8.0"])
    s.add_dependency(%q<rspec-mocks>, ["< 4.0.0", ">= 3.4.0"])
  end
end
