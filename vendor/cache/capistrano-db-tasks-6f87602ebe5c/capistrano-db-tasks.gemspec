# -*- encoding: utf-8 -*-
# stub: capistrano-db-tasks 0.4 ruby lib

Gem::Specification.new do |s|
  s.name = "capistrano-db-tasks"
  s.version = "0.4"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Sebastien Gruhier"]
  s.date = "2016-03-27"
  s.description = "A collection of capistrano tasks for syncing assets and databases"
  s.email = ["sebastien.gruhier@xilinus.com"]
  s.files = ["Gemfile", "LICENSE", "README.markdown", "Rakefile", "capistrano-db-tasks.gemspec", "lib/capistrano-db-tasks.rb", "lib/capistrano-db-tasks/asset.rb", "lib/capistrano-db-tasks/compressors/base.rb", "lib/capistrano-db-tasks/compressors/bzip2.rb", "lib/capistrano-db-tasks/compressors/gzip.rb", "lib/capistrano-db-tasks/database.rb", "lib/capistrano-db-tasks/dbtasks.rb", "lib/capistrano-db-tasks/util.rb", "lib/capistrano-db-tasks/version.rb", "test/capistrano_db_tasks_test.rb", "test/test_helper.rb"]
  s.homepage = "https://github.com/sgruhier/capistrano-db-tasks"
  s.rubyforge_project = "capistrano-db-tasks"
  s.rubygems_version = "2.4.6"
  s.summary = "A collection of capistrano tasks for syncing assets and databases"
  s.test_files = ["test/capistrano_db_tasks_test.rb", "test/test_helper.rb"]

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<capistrano>, [">= 3.0.0"])
    else
      s.add_dependency(%q<capistrano>, [">= 3.0.0"])
    end
  else
    s.add_dependency(%q<capistrano>, [">= 3.0.0"])
  end
end
