require 'rails_helper'

RSpec.describe Article, type: :model do
  describe 'slugged' do
    before(:each) do
      @article = Article.create name: 'Article 1', published: false
    end

    it 'recognizible name' do
      expect(@article.slug).to eq('article-1')
    end
  end

  describe 'flag_attrs' do
    before(:each) do
      @article = Article.create name: 'Article', published: false
    end

    it '.set_on' do
      @article.set_published_on
      @article.reload
      expect(@article.published).to eq(true)

      @article.set_published_off
      @article.reload
      expect(@article.published).to eq(false)

      Article.set_published_on
      @article.reload
      expect(@article.published).to eq(true)

      Article.set_published_off
      @article.reload
      expect(@article.published).to eq(false)

      Article.set_each_published_on
      @article.reload
      expect(@article.published).to eq(true)

      Article.set_each_published_off
      @article.reload
      expect(@article.published).to eq(false)
    end
  end

  describe 'nested set' do
    before(:each) do
      @first_article = Article.create name: 'first'
      @first_article.reload
    end

    it 'materialize path' do
      expect(@first_article.path).to eq('first')

      second_article = Article.create name: 'second'
      second_article.place_to @first_article.id
      second_article.reload
      expect(second_article.path).to eq('first/second')
    end

    it 'place to' do
      second_article = Article.create name: 'second'

      expect(second_article.lft).to eq(3)
      expect(second_article.rgt).to eq(4)
      expect(second_article.depth).to eq(0)
      expect(second_article.parent_id).to eq(nil)

      @first_article.place_to nil, second_article.id
      second_article.reload
      expect(second_article.lft).to eq(1)
      expect(second_article.rgt).to eq(2)

      second_article.place_to @first_article
      second_article.reload
      expect(second_article.depth).to eq(1)
      expect(second_article.parent_id).to eq(@first_article.id)

      @first_article.move_children_to_parent!
      second_article.reload
      expect(second_article.depth).to eq(0)
      expect(second_article.parent_id).to eq(nil)
    end

    it 'safe destroy' do
      second_article = Article.create name: 'second'
      second_article.place_to @first_article.id
      second_article.reload

      third_article = Article.create name: 'second'
      third_article.place_to second_article.id
      third_article.reload

      second_article.safe_destroy children_to: :parent
      third_article.reload
      expect(third_article.depth).to eq(1)
      expect(third_article.parent_id).to eq(1)

      @first_article.safe_destroy
      third_article.reload
      expect(third_article.depth).to eq(0)
      expect(third_article.parent_id).to eq(nil)

      Article.safe_destroy
      expect(Article.all).to eq([])
    end
  end
end
