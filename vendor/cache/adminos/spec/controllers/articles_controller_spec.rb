require 'rails_helper'

RSpec.describe ArticlesController, type: :controller do

  describe 'controller class methods' do
    describe '.resource' do
      before(:each) do
        @controller_class = controller.class
        @controller_class.resource Article
      end

      it '.index' do
        get :index
        expect(response).to be_success

        expect(controller.collection).to eq(Article.all)
      end

      it '.create' do
        article_params = { name: 'Article' }
        post :create, article: article_params
        expect(response).to be_redirect
      end

      it '.update' do
        article = Article.create name: 'Article'
        update_params = { name: 'Update' }

        put :update, id: article.id, article: update_params
        expect(response).to be_redirect

        article.reload
        expect(article.name).to eq(update_params[:name])
      end
    end
  end


  describe 'admin extensions' do
    before(:each) do
      @first_article = Article.create name: 'first', published: false
      @second_article = Article.create name: 'second', published: false
      @third_article = Article.create name: 'third', published: false
    end

    it '.batch_action destroy' do
      articles_ids = [@first_article.id, @second_article.id]
      post :batch_action, id_eq: articles_ids, destroy: true
      expect(response).to be_redirect

      expect(Article.where('id IN (?)', articles_ids).count).to eq(0)
    end

    it '.batch_action set_published on' do
      articles_ids = [@first_article.id, @second_article.id]

      post :batch_action, id_eq: articles_ids, set_published_on: true
      expect(response).to be_redirect

      @first_article.reload
      expect(@first_article.published).to eq(true)
    end

    it '.drop' do
      put :drop, id: @first_article.id, parent_id: @second_article.id
      expect(response).to be_redirect

      @first_article.reload
      expect(@first_article.parent_id).to eq(@second_article.id)
    end
  end
end
