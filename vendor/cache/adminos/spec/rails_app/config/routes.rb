Rails.application.routes.draw do

  scope module: :adminos do
    resource :session, only: [:new, :create, :destroy]
  end

  resources :articles do
    collection { post :batch_action }
    member { put :drop }
  end
end
