class Article < ActiveRecord::Base
  include Adminos::NestedSet::MaterializePath
  include Adminos::NestedSet::PlaceTo
  include Adminos::NestedSet::SafeDestroy
  include Adminos::FlagAttrs
  include Adminos::Slugged

  acts_as_nested_set
  materialize_path segment_accessor: :name
  flag_attrs :published
  slugged :name
end
