class AddNestedSetFieldsToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :parent_id, :integer, null: true, index: true
    add_column :articles, :lft, :integer, index: true
    add_column :articles, :rgt, :integer, index: true
    add_column :articles, :depth, :integer, default: 0
  end
end
