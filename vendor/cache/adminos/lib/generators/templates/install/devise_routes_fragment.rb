  devise_for :users, skip: :omniauth_callbacks
  devise_scope :user do
    get 'authentications/new', to: 'authentications#new'
    post 'authentications/link', to: 'authentications#link'
  end

