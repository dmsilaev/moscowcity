source 'https://rubygems.org'

# сервер, окружение
gem 'rails', '~> 4.2'

gem 'pg'
gem 'adminos', path: '../adminos'
gem 'therubyracer', platforms: :ruby

# html, js, css
gem 'haml'

gem 'jquery-rails'
gem 'autoprefixer-rails'
gem 'sprockets'
gem 'coffee-rails'
gem 'uglifier'

gem 'sass'
gem 'sass-rails'
gem 'bootstrap-sass-rails', '2.3.2.1'

# инструменты
gem 'configus' # конфигурационные данные
gem 'simple_form' # формы
gem 'carrierwave' # работа с файлами
gem 'mini_magick' # обработка изображений
gem 'russian' # для России
gem 'kaminari' # пагинация
gem 'draper' # паттерн "декоратор"
gem 'friendly_id' # слаги
gem 'babosa' # транслитерация слагов
gem 'sanitize' # санитайз
gem 'scoped_search'
gem 'redactor-rails', path: '../redactor-rails'

# авторизация, аутентификация
gem 'devise'
gem 'cancan'
gem 'role_model' # предлагаю поменять на Rolify в рамках рефакторинга
gem 'omniauth'
gem 'omniauth-facebook'
gem 'omniauth-vkontakte'
gem 'omniauth-linkedin'
gem 'omniauth-gplus', '~> 2.0'

# деплой
gem 'capistrano', require: false
gem 'capistrano-rails', require: false
gem 'capistrano3-unicorn', require: false
gem 'rvm1-capistrano3', require: false
gem 'capistrano-db-tasks', github: 'sgruhier/capistrano-db-tasks', require: false
gem 'airbrussh', require: false
gem 'awesome_nested_set', git: 'https://github.com/RavWar/awesome_nested_set', branch: 'adminos'  # они еще не пофиксили баг из форка Артура?

group :production do
  gem 'god', require: false
  gem 'unicorn'
end

group :development do
  gem 'better_errors'
  gem 'binding_of_caller', :platforms=>[:mri_19, :mri_20, :rbx]
  gem 'xray-rails'
end

group :development, :test do
  gem 'awesome_print'
  gem 'spring-commands-rspec' # прогоняем тесты через spring
  gem 'pry-rails'
  gem 'pry-coolline'
  gem 'pry-stack_explorer'
  gem 'faker'
  gem 'rspec-rails'
  gem 'database_cleaner'
  gem 'factory_girl_rails'
  gem 'guard-rspec', require: false
  gem 'i18n-tasks', '~> 0.7.11'

  # покрытие тестами
  gem 'simplecov', :require => false
  gem 'simplecov-console', :require => false
end