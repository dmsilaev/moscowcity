class Settings < ActiveRecord::Base
  before_validation :sanitize

  validates :company_name, :email, :email_header_from, :per_page, presence: true

  def self.get
    first || new
  end

  private

  def sanitize
    fields = [ :copyright, :email, :index_meta_description, :index_meta_title ]

    fields.each do |attribute|
      self[attribute] = Sanitize.clean self[attribute], elements: ['br']
    end
  end
end
