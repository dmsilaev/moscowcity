window.init_redactor = function(){
  var csrf_token = $('meta[name=csrf-token]').attr('content');
  var csrf_param = $('meta[name=csrf-param]').attr('content');
  var params;
  if (csrf_param !== undefined && csrf_token !== undefined) {
      params = csrf_param + "=" + encodeURIComponent(csrf_token);
  }
  $('.redactor').redactor({
        plugins: [
          'fontsize', 'fontcolor', 'fontfamily', 'filemanager',
          'fullscreen', 'clips', 'video', 'table', 'imagemanager'
        ],
        imageUpload:"/redactor_rails/pictures?" + params,
        imageGetJson:"/redactor_rails/pictures",
        imageManagerJson: "/redactor_rails/pictures",
        imageUploadErrorCallback: function(json){ alertErrors(json.error.data) },
        fileUpload:"/redactor_rails/documents?" + params,
        fileUploadErrorCallback: function(json){ alertErrors(json.error.data) },
        fileGetJson:"/redactor_rails/documents",
        path:"/assets/redactor-rails",
        css:"style.css",
        lang: $(this).data('lang') || 'ru'
      });
}

$(document).on( 'ready page:load', window.init_redactor );

alertErrors = function(array){
  $.each(array, function(i,v){
    alert(v);
  })
}