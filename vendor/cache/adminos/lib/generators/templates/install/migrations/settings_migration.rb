class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.string   :copyright
      t.string   :email
      t.string   :company_name
      t.string   :contact_email
      t.string   :email_header_from
      t.text     :index_meta_description
      t.string   :index_meta_title
      t.integer  :per_page, default: 10
      t.text     :seo_google_analytics
      t.text     :seo_yandex_metrika
    end

    Settings.reset_column_information
    Settings.first_or_initialize.update_attributes email: 'studio@molinos.ru',
      email_header_from: 'studio@molinos.ru', company_name: 'Molinos'
  end
end
