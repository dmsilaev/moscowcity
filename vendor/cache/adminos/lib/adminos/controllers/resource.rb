module Adminos::Controllers::Resource
  extend ActiveSupport::Concern

  module ClassMethods

    def resource klass, *args
      options = args.extract_options!
      finder           = options.delete(:finder)           || :find_by_id!
      location         = options.delete(:location)         || proc { nil }
      collection_scope = options.delete(:collection_scope) || nil
      filter_by_locale = options.delete(:filter_by_locale) || false
      parent_resource_klass = options.delete(:parent_resource)
      resource_instance        = options.delete(:resource_instance)
      parent_resource_instance = options.delete(:parent_resource_instance)
      with_parent_resource = parent_resource_klass.present? || parent_resource_instance.present?
      namespace = options.delete(:namespace)
      with_move_to = options.delete(:with_move_to)
      with_apply_sortable_order = options.delete(:with_apply_sortable_order)

      helper_method :resource, :collection, :resource_class, :resource_params
      helper_method(:parent_resource) if with_parent_resource

      define_method :create do
        if resource.save
          redirect_to action: :index
        else
          render :new
        end
      end

      define_method :update do
        if resource.update_attributes(strong_params)
          redirect_to action: :index
        else
          render :edit
        end
      end

      # NOTE: по-дефолту будет разрешено обновлять любые параметры
      define_method :strong_params do
        if params[resource_params].present?
          params.require(resource_params).permit!
        else
          {}
        end
      end

      protected

      define_method(:resource_class) { klass }

      define_method(:resource_params) { klass.name.pathalize }

      if with_parent_resource
        define_method :resource_as_association do
          self.resource_params.pluralize
        end

        define_method :parent_resource_params do
          parent_resource_klass.name.pathalize
        end
      end

      define_method :resource do
        return @resource if @resource.present?
        return(@resource = get_instance(resource_instance)) if resource_instance.present?

        @resource = if %w(new create).include?(action_name)
                      self.build_resource
                    else
                      self.find_resource
                    end
      end

      if with_parent_resource
        define_method :parent_resource do
          return @parent_resource if @parent_resource.present?
          return(@parent_resource = get_instance(parent_resource_instance)) if parent_resource_instance.present?

          @parent_resource = parent_resource_klass.
            find(params["#{parent_resource_params}_id"])
        end
      end

      define_method :collection do
        return @collection if @collection.present?

        collection  = resource_class
        collection = collection.with_translations(I18n.locale) if filter_by_locale

        return @collection = collection.where(nil) unless collection_scope.present?
        return @collection = collection.send(collection_scope) unless collection_scope.is_a?(Array)

        @collection = collection_scope.inject(collection) do |collection, method|
          collection.send(method)
        end
      end

      define_method :build_resource do
        self.resource_class_scope.new(strong_params)
      end

      define_method :find_resource do
        resource_class.send(finder, params[:id])
      end

      define_method :resource_class_scope do
        return parent_resource.send(self.resource_as_association) if with_parent_resource
        resource_class
      end

      define_method(:get_instance) { |instance| self.instance_eval(&instance) }

      if with_move_to || with_apply_sortable_order
        define_method :sort do
          if with_move_to
            resource.move_to(params[:to])
          elsif with_apply_sortable_order
            resource_class.apply_sortable_order(params[:id])
          end

          respond_to do |format|
            format.js { head :ok }
          end
        end
      end
    end
  end
end