require 'path'

module Adminos
  Helpers      = Module.new
  NestedSet    = Module.new
  Controllers  = Module.new
  StatefulLink = Module.new

  Path.require_tree 'adminos'

  class Engine < ::Rails::Engine
    initializer 'adminos.view_helpers' do
      ActionView::Base.send :include, Helpers::View
      ActionView::Base.send :include, Helpers::Admin
      ActionView::Base.send :include, Helpers::Bootstrap
    end

    initializer 'adminos.controller_helpers' do
      ActionController::Base.send :include, Controllers::Helpers
      ActionController::Base.send :include, Controllers::Resource
    end

    # initializer 'adminos.stateful_link' do
    #   ActionView::Base.send :include, StatefulLink::Helper
    #   ActionController::Base.send :include, StatefulLink::ActionAnyOf
    # end

    initializer 'adminos.assets.precompile' do |app|
      app.config.assets.precompile += %w(
        admin/wysiwyg.css
        admin/tinymce_config.js
        jquery.js
        adminos_gallery.js
        adminos_gallery.css
      )
    end
  end
end
