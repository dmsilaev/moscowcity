Gem::Specification.new do |gem|
  gem.name          = 'adminos'
  gem.version       = '4.0.0'
  gem.authors       = ['dmsilaev']
  gem.email         = ['dmsilaev@yandex.ru']
  gem.homepage      = ''
  gem.summary       = %q{Adminos molinos}
  gem.description   = %q{Molinos adminos}

  gem.files         = `git ls-files`.split($/)
  gem.test_files    = gem.files.grep %r{^spec/}
  gem.require_paths = %w(lib)

  gem.add_dependency 'path'
  gem.add_dependency 'rails', '~> 4.2'
  gem.add_dependency 'awesome_nested_set', '~> 3.0.2'

end
