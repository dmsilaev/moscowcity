require 'test_helper'

class AdminosGeneratorTest < Rails::Generators::TestCase
  destination DUMMY_PATH

  test 'Assert multiple types generation' do
    types = %w(default section sortable table)

    types.each do |type|
      name = "event_#{type}"

      unless File.exist? "#{DUMMY_PATH}/app/models/#{name}.rb"
        system %Q(cd #{DUMMY_PATH}; rails g adminos #{name.classify} --type=#{type})
      end

      assert_file "app/models/#{name}.rb"
      assert_file 'config/routes.rb', /resources :#{name.pluralize}, except: :show/
      assert_file 'app/views/shared/admin/_navbar.haml', /= top_menu_item active: 'admin\/#{name.pluralize}\#'/
      assert_file 'config/locales/ru.yml', /#{name.pluralize}:/
      assert_file "app/controllers/admin/#{name.pluralize}_controller.rb"
      assert_file "app/views/admin/#{name.pluralize}/_fields.haml"
      assert_file "app/views/admin/#{name.pluralize}/index.haml"
    end
  end
end
