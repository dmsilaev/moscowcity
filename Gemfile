source 'https://rubygems.org'

# сервер, окружение
gem 'rails', '~> 4.2'

gem 'pg'
gem 'adminos', path: './vendor/cache/adminos'
gem 'therubyracer', platforms: :ruby

# html
gem 'haml'
gem 'slim'

#assets
gem 'jquery-fileupload-rails'
gem 'jquery-ui-rails'
gem 'dropzonejs-rails'

gem 'paper_trail'
gem 'jquery-rails'
gem 'autoprefixer-rails'
gem 'sprockets'
gem 'coffee-rails'
gem 'uglifier'

gem 'safe_attributes'
gem 'sass'
gem 'sass-rails'
gem 'bootstrap-sass-rails', '2.3.2.1'

# инструменты
gem 'configus' # конфигурационные данные
gem 'simple_form' # формы
gem 'carrierwave' # работа с файлами
gem 'mini_magick' # обработка изображений
gem 'russian' # для России
gem "pundit"
gem 'money-rails', '~>1'
gem "reform-rails"
gem 'sorcery'
gem 'lograge' #логирование
gem 'kaminari' # пагинация
gem 'draper' # паттерн "декоратор"
gem 'friendly_id' # слаги
gem 'breadcrumbs' # хлебные крошки
gem 'enumerize'
gem 'babosa' # транслитерация слагов
gem 'sanitize' # санитайз
gem 'scoped_search'
gem 'concise_logging' # улучшенное логировние
gem 'redactor-rails', github: 'dmsilaev/redactor-rails'
# авторизация, аутентификация
# gem 'devise'
gem 'cancan'
gem 'role_model' # предлагаю поменять на Rolify в рамках рефакторинга'
gem "bower-rails", "~> 0.10.0"
# gem 'searchkick'
gem 'sunspot_rails'
gem 'sunspot_solr'
gem "react_on_rails", "~> 6.1"
gem 'activerecord-session_store' #сессия в базе
gem 'google_drive', github: 'gimite/google-drive-ruby'
gem 'telegram-bot'



#activeadmin
gem 'bootstrap-sass'
gem 'activeadmin', git: 'https://github.com/activeadmin/activeadmin'
gem 'active_bootstrap_skin'

# деплой
gem 'capistrano', require: false
gem 'capistrano-rails', require: false
gem 'capistrano3-unicorn', require: false
gem 'rvm1-capistrano3', require: false
gem 'capistrano-db-tasks', github: 'sgruhier/capistrano-db-tasks', require: false
gem 'airbrussh', require: false
gem 'awesome_nested_set'
gem 'ancestry'


group :production do
  gem 'god', require: false
  gem 'unicorn'
end

group :development do
  gem 'better_errors'
  gem 'binding_of_caller', :platforms=>[:mri_19, :mri_20, :rbx]
  gem 'xray-rails'
end

group :development, :test do
  gem 'awesome_print'
  gem 'spring-commands-rspec' # прогоняем тесты через spring
  gem 'pry-rails'
  gem 'pry-coolline'
  gem 'pry-stack_explorer'
  gem 'faker'
  gem 'rspec-rails'
  gem 'rspec-its'
  gem 'database_cleaner'
  gem 'factory_girl_rails'
  gem 'guard-rspec', require: false
  gem 'i18n-tasks', '~> 0.7.11'

  # покрытие тестами
  gem 'simplecov', :require => false
  gem 'simplecov-console', :require => false
end
